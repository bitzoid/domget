# domget

Will download <url> and have firefox parse the page, including all javascript.
Prints the dynamic DOM tree for further processing.

## Dependencies

Requires python3, python3-selenium, python3-wget. It will download geckodriver-v0.27 on its own and place it in its root directory.

## License

> domget
> Copyright (C) 2020  bitzoid
>
> This program is free software: you can redistribute it and/or modify
> it under the terms of the GNU General Public License as published by
> the Free Software Foundation, either version 3 of the License, or
> (at your option) any later version.
>
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU General Public License for more details.
>
> You should have received a copy of the GNU General Public License
> along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Author

*  bitzoid {zoid at riseup dot net}

